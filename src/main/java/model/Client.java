package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Client")
public class Client extends EntityData implements Serializable {

//  Atributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
//    Relations    
    @OneToOne(mappedBy = "client")
    AccountPlan accountPlan;
    @OneToMany(mappedBy = "client")
    private List<GrainTicket> grainTickets;

//    Methods
    /**
     *
     * @param name
     * @param accountPlan
     */
    public Client(String name, AccountPlan accountPlan) {
        this.accountPlan = accountPlan;
        this.name = name;
    }

    /**
     *
     * @param name
     * @param tipoDoc
     * @param cuit
     * @param zipCode
     * @param address
     * @param phone
     * @param condFiscal
     * @param email
     * @param iva
     */
    public Client(String name, IdType tipoDoc, String cuit, String zipCode, String address, String phone, float iva, FiscCondition condFiscal, String email) {
        this.name = name.toUpperCase();
        this.tipoDoc = tipoDoc;
        this.cuit = cuit;
        this.zipCode = zipCode;
        this.address = address;
        this.phone = phone;
        this.condFiscal = condFiscal;
        this.email = email;
        this.iva = iva;
    }

    /**
     *
     * @return
     */
    public AccountPlan getAccountPlan() {
        return accountPlan;
    }

    /**
     *
     * @param accountPlan
     */
    public void setAccountPlan(AccountPlan accountPlan) {
        this.accountPlan = accountPlan;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public List<GrainTicket> getGrainTickets() {
        return grainTickets;
    }

    /**
     *
     * @param grainTickets
     */
    public void setGrainTickets(List<GrainTicket> grainTickets) {
        this.grainTickets = grainTickets;
    }
}
