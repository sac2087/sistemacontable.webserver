/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Enterprise")
public class Enterprise extends EntityData implements Serializable {

//  Atributes    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
//    Relations
    @OneToMany(mappedBy = "enterprise")
    List<Ticket> tickets;
    @OneToMany(mappedBy = "enterprise")
    private List<EnterpriseAccount> enterpriseAccounts;

    /**
     *
     */
    public Enterprise() {
    }

    //    Methods     
    /**
     *
     * @param name
     * @param cuit
     * @param zipCode
     * @param address
     * @param phone
     * @param condFiscal
     * @param iva
     * @param email
     * @param tipoDoc
     * @param enterpriseAccounts
     */
    public Enterprise(String name, String cuit, String zipCode, String address, String phone, FiscCondition condFiscal, float iva, String email, IdType tipoDoc, List<EnterpriseAccount> enterpriseAccounts) {
        this.name = name.toUpperCase();
        this.cuit = cuit;
        this.zipCode = zipCode;
        this.address = address;
        this.phone = phone;
        this.condFiscal = condFiscal;
        this.iva = iva;
        this.email = email;
        this.tipoDoc = tipoDoc;
        this.enterpriseAccounts = enterpriseAccounts;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public List<EnterpriseAccount> getEnterpriseAccount() {
        return enterpriseAccounts;
    }

    /**
     *
     * @param accounts
     */
    public void setEnterpriseAccount(List<EnterpriseAccount> accounts) {
        this.enterpriseAccounts = accounts;
    }

    /**
     *
     * @param a
     */
    public void addEnterpriseAccount(EnterpriseAccount a) {
        enterpriseAccounts.add(a);
    }

    /**
     *
     * @param a
     */
    public void removeEnterpriseAccount(EnterpriseAccount a) {
        enterpriseAccounts.remove(a);
    }

    /**
     *
     * @param a
     */
    public void addTicket(Ticket a) {
        tickets.add(a);
    }

    /**
     *
     * @param a
     */
    public void removeTicket(Ticket a) {
        tickets.remove(a);
    }

    @Override
    public String toString() {
        return name;
    }
}
