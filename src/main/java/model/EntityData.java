package model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

/**
 *
 * @author lprone
 */
@MappedSuperclass
public class EntityData implements Serializable {

//  Atributes
    @Basic
    String name;
    @Column(unique = true)
    @Basic
    String cuit;
    @Basic
    String zipCode;
    @Basic
    String address;
    @Basic
    String phone;
    @Basic
    FiscCondition condFiscal;
    @Basic
    float iva;
    @Basic
    String email;
    IdType tipoDoc;
//    Methods    

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     */
    public String getCuit() {
        return cuit;
    }

    /**
     *
     * @param cuit
     */
    public void setCuit(String cuit) {
        this.cuit = cuit;
    }

    /**
     *
     * @return
     */
    public String getAddress() {
        return address;
    }

    /**
     *
     * @param address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     *
     * @return
     */
    public String getPhone() {
        return phone;
    }

    /**
     *
     * @param phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     *
     * @return
     */
    public FiscCondition getCondFiscal() {
        return condFiscal;
    }

    /**
     *
     * @param condFiscal
     */
    public void setCondFiscal(FiscCondition condFiscal) {
        this.condFiscal = condFiscal;
    }

    /**
     *
     * @return
     */
    public float getIva() {
        return iva;
    }

    /**
     *
     * @param iva
     */
    public void setIva(float iva) {
        this.iva = iva;
    }

    /**
     *
     * @return
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @param tipoDoc
     */
    public void setTipoDoc(IdType tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    /**
     *
     * @return
     */
    public IdType getTipoDoc() {
        return tipoDoc;
    }

    @Override
    public String toString() {
        return cuit + " - " + name;
    }

    /**
     *
     * @return
     */
    public String getZipCode() {
        return zipCode;
    }

    /**
     *
     * @param zipCode
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}
