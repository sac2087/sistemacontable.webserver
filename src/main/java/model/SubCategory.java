package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "SubCategory")
public class SubCategory implements Serializable {

//  Atributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(unique = true)
    private String name;
//    Relations    
    @Basic
    @ManyToOne
    private Category cat;
    @OneToMany(mappedBy = "subCat", cascade = CascadeType.REMOVE)
    private List<Item> items;

//    Methods      
    /**
     *
     * @param name
     */
    public SubCategory(String name) {
        this.name = name.toUpperCase();
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    /**
     *
     * @return
     */
    public Category getCat() {
        return cat;
    }

    /**
     *
     * @param cat
     */
    public void setCat(Category cat) {
        this.cat = cat;
    }

    /**
     *
     * @return
     */
    public List<Item> getItems() {
        return items;
    }

    /**
     *
     * @param items
     */
    public void setItems(List<Item> items) {
        this.items = items;
    }

    /**
     *
     * @param i
     */
    public void addItem(Item i) {
        items.add(i);
    }

    /**
     *
     * @param i
     */
    public void removeItem(Item i) {
        items.remove(i);
    }

    @Override
    public String toString() {
        return name;
    }
}
