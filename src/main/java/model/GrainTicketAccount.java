/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Grain_Ticket_Account")
public class GrainTicketAccount implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    BigDecimal price;
    float ivaPervent;
    int kg;
    GrainType grainType;
//    Relations
    @ManyToOne
    Account account;
    @ManyToOne
    GrainTicket ticket;
    @Basic
    TConcepto concept;

    //    Methods
    /**
     *
     */
    public GrainTicketAccount() {
    }

    /**
     *
     * @param price
     * @param ivaPervent
     * @param kg
     * @param grainType
     * @param account
     * @param ticket
     * @param concept
     */
    public GrainTicketAccount(BigDecimal price, float ivaPervent, int kg, GrainType grainType, Account account, GrainTicket ticket, TConcepto concept) {
        this.price = price;
        this.ivaPervent = ivaPervent;
        this.kg = kg;
        this.grainType = grainType;
        this.account = account;
        this.ticket = ticket;
        this.concept = concept;
    }

    /**
     *
     * @return
     */
    public Account getAccount() {
        return account;
    }

    /**
     *
     * @param account
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     *
     * @return
     */
    public GrainTicket getTicket() {
        return ticket;
    }

    /**
     *
     * @param ticket
     */
    public void setTicket(GrainTicket ticket) {
        this.ticket = ticket;
    }

    /**
     *
     * @return
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     *
     * @param price
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     */
    public TConcepto getConcepto() {
        return concept;
    }

    /**
     *
     * @param concept
     */
    public void setConcepto(TConcepto concept) {
        this.concept = concept;
    }

    /**
     *
     * @return
     */
    public float getIvaPervent() {
        return ivaPervent;
    }

    /**
     *
     * @param ivaPervent
     */
    public void setIvaPervent(float ivaPervent) {
        this.ivaPervent = ivaPervent;
    }

    /**
     *
     * @return
     */
    public TConcepto getConcept() {
        return concept;
    }

    /**
     *
     * @param concept
     */
    public void setConcept(TConcepto concept) {
        this.concept = concept;
    }

    /**
     *
     * @return
     */
    public int getKg() {
        return kg;
    }

    /**
     *
     * @param kg
     */
    public void setKg(int kg) {
        this.kg = kg;
    }

    /**
     *
     * @return
     */
    public GrainType getGrainType() {
        return grainType;
    }

    /**
     *
     * @param grainType
     */
    public void setGrainType(GrainType grainType) {
        this.grainType = grainType;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "";
    }
}
