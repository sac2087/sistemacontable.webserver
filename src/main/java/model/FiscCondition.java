/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lprone
 */
public enum FiscCondition {

    /**
     *
     */
    IVA_RESPONSABLE_INSCRIPTO,
    /**
     *
     */
    NO_RESPONSABLE,
    /**
     *
     */
    EXCENTO,
    /**
     *
     */
    MONOTRIBUTO,
    /**
     *
     */
    MONOTRIBUTO_SOCIAL,
    /**
     *
     */
    MONOTRIBUTO_EVENTUAL,
    /**
     *
     */
    CONSUMIDOR_FINAL
}
