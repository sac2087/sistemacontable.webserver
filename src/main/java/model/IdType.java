/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import static model.IdType.*;

/**
 *
 * @author lprone
 */
public enum IdType {

    /**
     *
     */
    CUIT,
    /**
     *
     */
    CUIL,
    /**
     *
     */
    DNI,
    /**
     *
     */
    CDI,
    /**
     *
     */
    PASAPORTE,
    /**
     *
     */
    LC,
    /**
     *
     */
    LE,
    /**
     *
     */
    ACTA_DE_NACIMIENTO,
    /**
     *
     */
    EN_TRAMITE;

    /**
     *
     * @param id
     * @return
     */
    public static int valueOf(IdType id) {
        switch (id) {
            case ACTA_DE_NACIMIENTO:
                return 93;
            case CDI:
                return 87;
            case CUIL:
                return 86;
            case CUIT:
                return 80;
            case DNI:
                return 96;
            case EN_TRAMITE:
                return 92;
            case LC:
                return 90;
            case LE:
                return 89;
            case PASAPORTE:
                return 94;
            default:
                return -1;
        }
    }
}
