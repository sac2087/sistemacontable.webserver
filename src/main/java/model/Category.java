package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Category")
public class Category implements Serializable {

//  Atributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(unique = true)
    private String name;
//    Relations    
    @OneToMany(mappedBy = "cat", cascade = CascadeType.REMOVE)
    private List<SubCategory> subCat;

//    Methods    
    /**
     *
     * @param name
     */
    public Category(String name) {
        this.name = name.toUpperCase();
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     */
    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    /**
     *
     * @return
     */
    public List<SubCategory> getSubCat() {
        return subCat;
    }

    /**
     *
     * @param subCat
     */
    public void setSubCat(List<SubCategory> subCat) {
        this.subCat = subCat;
    }

    /**
     *
     * @param sc
     */
    public void addSubCat(SubCategory sc) {
        subCat.add(sc);
    }

    /**
     *
     * @param sc
     */
    public void removeSubCat(SubCategory sc) {
        subCat.remove(sc);
    }

    @Override
    public String toString() {
        return name;
    }
}
