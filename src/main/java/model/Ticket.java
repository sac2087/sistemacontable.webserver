package model;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Ticket")
public class Ticket implements Serializable, Comparable {

//  Atributes
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Basic
    private Date emissionDate;
    @Basic
    private Date contDate;
    @Basic
    private Date impFDate;
    @Basic
    private TOperacion tOp;
    @Basic
    private String nro;
//    Relations    
    @ManyToOne
    private TTicket type;
    @ManyToOne
    private Enterprise enterprise;
    @OneToMany(mappedBy = "ticket")
    private List<TicketAccount> ticket_Accounts;

//    Methods
    /**
     *
     */
    public Ticket() {
    }

    /**
     * Ticket comercio
     *
     * @param number
     * @param emissionDate
     * @param contDate
     * @param impFDate
     * @param type
     * @param enterprise
     * @param tOp
     */
    public Ticket(String number, Date emissionDate, Date contDate, Date impFDate, TTicket type, Enterprise enterprise, TOperacion tOp) {
        this.nro = number;
        this.emissionDate = emissionDate;
        this.contDate = contDate;
        this.impFDate = impFDate;
        this.type = type;
        this.enterprise = enterprise;
        this.tOp = tOp;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getNumber() {
        return nro.substring(4, 12);
    }

    /**
     *
     * @param number
     */
    public void setNumber(String number) {
        this.nro = number;
    }

    /**
     *
     * @return
     */
    public String getLote() {
        return nro.substring(12, 16);
    }

    /**
     *
     * @return
     */
    public String getFullNro() {
        return nro;
    }

    /**
     *
     * @return
     */
    public String getSucursal() {
        return nro.substring(0, 4);
    }

    /**
     *
     * @return
     */
    public Date getEmissionDate() {
        return emissionDate;
    }

    /**
     *
     * @param date
     */
    public void setEmissionDate(Date date) {
        this.emissionDate = date;
    }

    /**
     *
     * @return
     */
    public Date getContDate() {
        return contDate;
    }

    /**
     *
     * @return
     */
    public Date getImpFDate() {
        return impFDate;
    }

    /**
     *
     * @param contDate
     */
    public void setContDate(Date contDate) {
        this.contDate = contDate;
    }

    /**
     *
     * @param impFDate
     */
    public void setImpFDate(Date impFDate) {
        this.impFDate = impFDate;
    }

    /**
     *
     * @return
     */
    public TTicket getType() {
        return type;
    }

    /**
     *
     * @param type
     */
    public void setType(TTicket type) {
        this.type = type;
    }

    /**
     *
     * @param enterprise
     */
    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    /**
     *
     * @return
     */
    public Enterprise getEnterprise() {
        return enterprise;
    }

    /**
     *
     * @return
     */
    public TOperacion gettOp() {
        return tOp;
    }

    /**
     *
     * @param tOp
     */
    public void settOp(TOperacion tOp) {
        this.tOp = tOp;
    }

    /**
     *
     * @return
     */
    public List<TicketAccount> getTicket_Accounts() {
        return ticket_Accounts;
    }

    /**
     *
     * @param ticket_Accounts
     */
    public void setTicket_Accounts(List<TicketAccount> ticket_Accounts) {
        this.ticket_Accounts = ticket_Accounts;
    }

    @Override
    public String toString() {
        return String.valueOf(type) + nro;
    }

    public int compareTo(Object o) {
        return this.emissionDate.compareTo(((Ticket) o).emissionDate);
    }
}
