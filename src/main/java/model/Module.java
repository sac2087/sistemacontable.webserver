/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.jdo.annotations.PrimaryKey;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author lprone
 */
@Entity
public class Module implements Serializable {

    @Id
    @PrimaryKey
    int id;
    @Basic
    String description = null;
    @Basic
    boolean enable;
    @Basic
    Float version;
    @Basic
    String path;

    /**
     *
     * @param id
     * @param description
     * @param enable
     * @param version
     * @param path
     */
    public Module(int id, String description, boolean enable, Float version, String path) {
        this.id = id;
        this.description = description;
        this.enable = enable;
        this.version = version;
        this.path = path;
    }

    /**
     *
     */
    public Module() {
    }

    @Override
    public String toString() {
        return description + " - " + version;
    }

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @return
     */
    public String getDescription() {
        return description;
    }

    /**
     *
     * @return
     */
    public String getPath() {
        return path;
    }

    /**
     *
     * @return
     */
    public boolean isEnable() {
        return enable;
    }

    /**
     *
     * @return
     */
    public Float getVersion() {
        return version;
    }

    /**
     *
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *
     * @param enable
     */
    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    /**
     *
     * @param version
     */
    public void setVersion(float version) {
        this.version = version;
    }

    /**
     *
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     *
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }
}
