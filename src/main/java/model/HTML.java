package model;

import java.util.List;

/**
 *
 * @author lprone
 */
public class HTML {

    /**
     * Generic show module
     *
     * @param data
     * @return
     */
    public static String show(List data) {
        String ret;
        ret = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n"
                + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n"
                + "<head>\n"
                + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" />\n"
                + "<title>Listar</title>\n"
                + "</head>\n"
                + "<body style=\"background-color:#eeeeee;\">\n";
        for (Object o : data) {
            ret += "<p>" + o.toString() + "</p>\n";
        }
        ret += "</body>\n"
                + "</html>";
        return ret;
    }

    /**
     * HTML code for login page
     *
     * @return
     */
    public static String login() {
        return "<!DOCTYPE html>\n"
                + "<html lang=\"en\">\n"
                + "  <head>\n"
                + "    <meta charset=\"utf-8\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "    <meta name=\"description\" content=\"\">\n"
                + "    <meta name=\"author\" content=\"\">\n"
                + "    <link rel=\"shortcut icon\" href=\"http://getbootstrap.com/assets/ico/favicon.ico\">\n"
                + "\n"
                + "    <title>Sistema Contable</title>\n"
                + "\n"
                + "    <!-- Bootstrap core CSS -->\n"
                + "    <link href=\"http://getbootstrap.com/dist/css/bootstrap.min.css\" rel=\"stylesheet\">\n"
                + "\n"
                + "    <!-- Custom styles for this template -->\n"
                + "    <link href=\"http://getbootstrap.com/examples/signin/signin.css\" rel=\"stylesheet\">\n"
                + "\n"
                + "    <!-- Just for debugging purposes. Don't actually copy this line! -->\n"
                + "    <!--[if lt IE 9]><script src=\"../../assets/js/ie8-responsive-file-warning.js\"></script><![endif]-->\n"
                + "\n"
                + " <link href=\"http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/css/bootstrap-editable.css\" rel=\"stylesheet\"/>\n"
                + "<script src=\"http://cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js\"></script>\n"
                + "    <![endif]-->\n"
                + "  </head>\n"
                + "\n"
                + "  <body>\n"
                + "\n"
                + "    <div class=\"container\">\n"
                + "\n"
                + "      <form class=\"form-signin\" role=\"form\" method=\"post\">\n"
                + "        <h2 class=\"form-signin-heading\" align=\"center\">Sistema Contable</h2>\n"
                + "        <input id=\"user\" name=\"user\" type=\"text\" class=\"form-control\" placeholder=\"Usuario\" required autofocus>\n"
                + "        <input id=\"password\" name=\"password\" type=\"password\" class=\"form-control\" placeholder=\"Clave\" required>\n"
                + "        \n"
                + "        <button class=\"btn btn-lg btn-primary btn-block\" type=\"submit\">Ingresar</button>\n"
                + "      </form>\n"
                + "\n"
                + "    </div> <!-- /container -->\n"
                + "\n"
                + "\n"
                + "    <!-- Bootstrap core JavaScript\n"
                + "    ================================================== -->\n"
                + "    <!-- Placed at the end of the document so the pages load faster -->\n"
                + "  </body>\n"
                + "</html>";
    }

    /**
     * Head of pages
     *
     * @param title
     * @return
     */
    private static String head(String title) {
        String prueba = "http://www.sampahost.com.ar/sistemawebTesis/";
        String bootstrapCss = prueba + "css/bootstrap.min.css";
        String bootstrapThemeCss = prueba + "css/bootstrap-theme.min.css";
        String bootstrapEditCss = prueba + "css/bootstrap-editable.css";
        String bootstrapJs = prueba + "js/bootstrap.min.js";
        String bootstrapEditJs = prueba + "js/bootstrap-editable.min.js";
        String bootstrapUserAttributesJs = prueba + "js/userAttributes.js";
        String jQueryJs = prueba + "js/jquery-2.1.0.min.js";
        return "<!DOCTYPE html>\n"
                + "<html lang=\"es\">\n"
                + "  <head>\n"
                + "    <meta charset=\"utf-8\">\n"
                + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
                + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n"
                + "    <title>" + title + "</title>\n"
                + "    <script src=\"" + jQueryJs + "\"></script>\n"
                + "    <script src=\"" + bootstrapJs + "\"></script>\n"
                + "    <script src=\"" + bootstrapEditJs + "\"></script>\n"
                + "    <script src=\"" + bootstrapUserAttributesJs + "\"></script>\n"
                + "    <!-- Bootstrap core CSS -->\n"
                + "    <link href=\"" + bootstrapCss + "\" rel=\"stylesheet\">\n"
                + "    <link href=\"" + bootstrapThemeCss + "\" rel=\"stylesheet\">\n"
                + "    <link href=\"" + bootstrapEditCss + "\" rel=\"stylesheet\">\n"
                + "\n";
    }

    /**
     * HTML code for options bar
     *
     * @return
     */
    private static String topBar() {
        return "     <nav class=\"navbar navbar-default\" role=\"navigation\">\n"
                + "         <div class=\"container-fluid\">\n"
                + "             <div class=\"navbar-header\">\n"
                + "                 <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-9\">\n"
                + "                     <span class=\"sr-only\">Toggle navigation</span>\n"
                + "                     <span class=\"icon-bar\"></span>\n"
                + "                     <span class=\"icon-bar\"></span>\n"
                + "                     <span class=\"icon-bar\"></span>\n"
                + "                 </button>\n"
                + "                 <a class=\"navbar-brand\" href=\"main\">Sistema Contable IVA</a>\n"
                + "             </div>\n"
                + "             <ul class=\"nav navbar-nav\">\n"
                + "                 <li><a href=\"./users\">Usuarios</a></li>\n"
                + "                 <li><a href=\"./modules\">Modulos</a></li>\n"
                + "                 <li><a href=\"./reports\">Reportes</a></li>\n"
                + "             </ul>\n"
                + "             <div class=\"collapse navbar-collapse\" id=\"bs-example-navbar-collapse-9\">\n"
                + "               <ul class=\"nav navbar-nav navbar-right\">\n"
                + "                   <li><a href=\"./logout\">Cerrar Sesion</a></li>\n"
                + "               </ul>"
                + "             </div>\n"
                + "         </div>\n"
                + "     </nav>\n";
    }

    /**
     * HTML code for users list
     *
     * @param allUsers
     * @return
     */
    public static String Users(List allUsers) {
        return head("Modulos")
                + "</head>\n"
                + "<body>\n"
                + topBar()
                + show(allUsers);
    }

    /**
     * HTML code for index page
     *
     * @return
     */
    public static Object main() {
        return head("Sistema Contable")
                + "</head>\n"
                + "<body>\n"
                + topBar();
    }

    /**
     * HTML code for reports download page
     *
     * @param clients
     * @return
     */
    public static String Reports(List clients) {
        String ret = head("Informes")
                + "</head>\n"
                + "<body>\n"
                + topBar()
                + "<form class=\"form-horizontal\" method=\"post\">\n"
                + "  <fieldset>\n"
                + "<div class=\"row\">\n"
                + "          <div class=\"col-lg-6\">\n"
                + "            <div class=\"well bs-component\">"
                + "    <div class=\"form-group\">\n"
                + "      <label class=\"col-lg-2 control-label\">Libro Iva</label>\n"
                + "      <div class=\"col-lg-10\">\n"
                + "        <div class=\"radio\">\n"
                + "          <label>\n"
                + "            <input type=\"radio\" name=\"ivabook\" id=\"optionsRadios1\" value=\"compras\" checked=\"\">\n"
                + "            Compras\n"
                + "          </label>\n"
                + "        </div>\n"
                + "        <div class=\"radio\">\n"
                + "          <label>\n"
                + "            <input type=\"radio\" name=\"ivabook\" id=\"optionsRadios2\" value=\"ventas\">\n"
                + "            Ventas\n"
                + "          </label>\n"
                + "        </div>\n"
                + "      </div>\n"
                + "    </div>\n"
                + "    <div class=\"form-group\">\n"
                + "      <label for=\"select\" class=\"col-lg-2 control-label\">Cliente</label>\n"
                + "      <div class=\"col-lg-10\">\n"
                + "        <select class=\"form-control\" id=\"client\" name=\"client\">\n";

        for (Object c : clients) {
            Client client = (Client) c;
            ret += "<option value=\"" + client.getCuit() + "\">" + client.getName() + "</option>\n";
        }

        ret += "        </select>\n"
                + "        <br>\n"
                + "      </div>\n"
                + "    </div>\n"
                + "    <div class=\"form-group\">\n"
                + "      <div class=\"col-lg-10 col-lg-offset-2\">\n"
                + "        <button type=\"submit\" class=\"btn btn-primary\">Descargar</button>\n"
                + "      </div>\n"
                + "    </div>\n"
                + "  </fieldset>\n"
                + "</form>"
                + "          </div>\n"
                + "        </div>\n"
                + "      </div>";
        return ret;
    }

    /**
     * HTML code for modules list
     *
     * @param modules
     * @return
     */
    public static Object Modules(List modules) {
        return head("Modulos")
                + "</head>\n"
                + "<body>\n"
                + topBar()
                + show(modules);
    }
}
