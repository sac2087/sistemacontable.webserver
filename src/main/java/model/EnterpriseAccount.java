/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author lprone
 */
@Entity
@Table(name = "Enterprise_Account")
public class EnterpriseAccount implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    Enterprise enterprise;
    @ManyToOne
    Account account;

    /**
     *
     */
    public EnterpriseAccount() {
    }

    /**
     *
     * @param enterprise
     * @param account
     */
    public EnterpriseAccount(Enterprise enterprise, Account account) {
        this.enterprise = enterprise;
        this.account = account;
    }

    /**
     *
     * @return
     */
    public Account getAccount() {
        return account;
    }

    /**
     *
     * @return
     */
    public Enterprise getEnterprise() {
        return enterprise;
    }

    /**
     *
     * @param account
     */
    public void setAccount(Account account) {
        this.account = account;
    }

    /**
     *
     * @param enterprise
     */
    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return enterprise.getId() + " - " + account.getId();
    }
}
