package system;

import controller.ClientController;
import controller.LoginController;
import controller.ModuleController;
import controller.UserController;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import javax.swing.JOptionPane;
import model.Client;
import model.HTML;
import model.TOperacion;
import static spark.Spark.*;
import spark.*;

/**
 *
 * @author lprone
 */
public class Main {

//    private static final String path = (System.getProperty("user.dir") + "\\");
    private static final String path = (System.getProperty("user.dir") + "/Modulos/Web Server/");

    /**
     * Verify if user has active session
     *
     * @param rqst
     * @return
     */
    private static boolean CheckAccess(Request rqst) {
        try {
            return rqst.session().attribute("login") == "true";
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Delete old reports
     */
    private static void clean() {
        try {
//            JOptionPane.showMessageDialog(null,  "\'" + path + "Cleaner.bat" + "\'");
            Runtime.getRuntime().exec(new String[]{"cmd", "/c", path + "Cleaner.bat"});
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Error en el clean");
        }
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {

        JOptionPane.showMessageDialog(null, "Servidor Activo");
        clean();
        setPort(80);

        /**
         * Default get route
         */
        get(new Route("sistemacontable") {
            @Override
            public Object handle(Request rqst, Response rspns) {
                if (CheckAccess(rqst)) {
                    rspns.redirect("main");
                } else {
                    rspns.redirect("login");
                }
                return null;
            }
        });

        /**
         * Test route
         */
        get(new Route("/hello") {
            @Override
            public Object handle(Request request, Response response) {
                if (CheckAccess(request)) {
                    return "Hello";
                }
                response.redirect("login");
                return null;
            }
        });

        /**
         * Index route
         */
        get(new Route("/main") {
            @Override
            public Object handle(Request rqst, Response rspns) {
                if (CheckAccess(rqst)) {
                    return HTML.main();
                }
                rspns.redirect("login");
                return null;
            }
        });

        /**
         * Login route
         */
        get(new Route("/login") {
            @Override
            public Object handle(Request rqst, Response rspns) {
                if (CheckAccess(rqst)) {
                    rspns.redirect("main");
                    return null;
                } else {
                    return HTML.login();
                }
            }
        });

        /**
         *
         */
        post(new Route("/login") {
            @Override
            public Object handle(Request request, Response response) {
                String user = request.queryParams("user");
                String password = request.queryParams("password");
                if (LoginController.validate(user, password)) {
                    request.session(true);
                    request.session().attribute("login", "true");
                    response.redirect("main");
                    return null;
                } else {
                    return "<script type=\"text/javascript\">alert(\"Datos Incorrectos!\");history.back();</script>\n";
                }
            }
        });

        /**
         * Route to get users
         */
        get(new Route("/users") {
            @Override
            public Object handle(Request request, Response response) {
                if (CheckAccess(request)) {
                    return HTML.Users(UserController.getAllUsers());
                }
                response.redirect("login");
                return null;
            }
        });

        /**
         *
         */
        get(new Route("/reports") {
            @Override
            public Object handle(Request request, Response response) {
                if (CheckAccess(request)) {
                    return HTML.Reports(ClientController.getAll());
                }
                response.redirect("login");
                return null;
            }
        });

        /**
         *
         */
        post(new Route("reports") {
            @Override
            public Object handle(Request request, Response response) {
                if (CheckAccess(request)) {
                    String clientCuit = request.queryParams("client");
                    String book = request.queryParams("ivabook");
                    Client client = ClientController.findForDoc(clientCuit);
                    String filename = "IVA_" + book.toUpperCase() + "_" + clientCuit + "_" + Calendar.getInstance().getTimeInMillis();
                    String fullPath = path + "reports/" + filename + ".pdf";
                    if (book.compareTo("compras") == 0) {
                        new reports.IVABook().createPdf(fullPath, client, TOperacion.COMPRA);
                    } else {
                        new reports.IVABook().createPdf(fullPath, client, TOperacion.VENTA);
                    }
                    response.redirect("download?file=" + filename);
                }
                return null;
            }
        });

        /**
         *
         */
        get(new Route("download") {
            @Override
            public Object handle(Request request, Response response) {
                String filename = request.queryParams("file") + ".pdf";
                File pdfFile = new File(path + "reports/" + filename);
                try {
                    OutputStream responseOutputStream = response.raw().getOutputStream();
                    FileInputStream fileInputStream = new FileInputStream(pdfFile);
                    response.type("application/pdf");
                    response.header("Content-Disposition", "attachment; filename=" + filename);
                    int bytes;
                    while ((bytes = fileInputStream.read()) != -1) {
                        responseOutputStream.write(bytes);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                pdfFile.deleteOnExit();
                return "Descargando";
            }
        });

        /**
         *
         */
        get(
                new Route("/modules") {
            @Override
            public Object handle(Request request, Response response) {
                if (CheckAccess(request)) {
                    return HTML.Modules(ModuleController.getAllModules());
                }
                response.redirect("login");
                return null;

            }
        });

        /**
         *
         */
        get(
                new Route("logout") {
            @Override
            public Object handle(Request rqst, Response rspns) {
                rqst.session().removeAttribute("login");
                rqst.session(false);
                return "Sesion cerrada";
            }
        });


        /**
         *
         */
        get(new Route("close") {
            @Override
            public Object handle(Request request, Response response) {
                clean();
                System.exit(0);
                return "";
            }
        });
    }
}
