/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.math.BigDecimal;
import model.Account;
import model.GrainTicket;
import model.GrainTicketAccount;
import model.GrainType;
import model.TConcepto;
import model.Ticket;
import model.TicketAccount;

/**
 *
 * @author lprone
 */
public class GrainTicketAccountController {

    /**
     *
     * @param t
     * @param tc
     * @return
     */
    public static GrainTicketAccount get(GrainTicket t, TConcepto tc) {
        for (GrainTicketAccount ta : t.getTicket_Accounts()) {
            if (ta.getConcept() == tc) {
                return ta;
            }
        }
        return null;
    }

    /**
     *
     * @param ticketAccount
     * @return
     */
    public static boolean update(GrainTicketAccount ticketAccount) {
        boolean result;
        try {
            JPA.beginTransaction();
            result = JPA.update(ticketAccount);
            JPA.commitTransaction();
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return result;
    }
}
