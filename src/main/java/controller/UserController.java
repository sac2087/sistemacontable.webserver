/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.User;

/**
 *
 * @author gdf
 */
public class UserController {

    /**
     *
     * @return
     */
    public static List getAllUsers() {
        return JPA.getAll(User.class);
    }
}
