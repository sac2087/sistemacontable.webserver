/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.List;
import model.Module;

/**
 *
 * @author lprone
 */
public class ModuleController {

    /**
     *
     * @return
     */
    public static List getAllModules() {
        return JPA.getAll(Module.class);
    }
}