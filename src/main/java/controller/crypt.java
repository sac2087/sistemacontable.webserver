/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/**
 * Encrypt and decrypt data
 *
 * @author lprone
 */
public class crypt {

    private static String pass = "qwertyuiop123456";
    private static SecretKey key;
    private static Cipher cipher;

    static {
        try {
            key = new SecretKeySpec(pass.getBytes(), "AES");
            cipher = Cipher.getInstance("AES/ECB/PKCS5Padding", "SunJCE");
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

    /**
     *
     * @param plainText
     * @return encrypted text string
     * @throws Exception
     */
    public synchronized String encrypt(String plainText) throws Exception {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        byte[] cipherText = cipher.doFinal(plainText.getBytes());
        return Base64.encode(cipherText);
    }

    /**
     *
     * @param codedText
     * @return decrypted text string
     * @throws Exception
     */
    public synchronized String decrypt(String codedText) throws Exception {
        byte[] encypted = Base64.decode(codedText);
        cipher.init(Cipher.DECRYPT_MODE, key);
        byte[] decrypted = cipher.doFinal(encypted);
        return new String(decrypted);
    }
}