/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import model.Client;
import model.Enterprise;
import model.GrainTicket;

/**
 *
 * @author lprone
 */
public class GrainTicketController {

    /**
     *
     * @param gs
     * @return
     */
    public static boolean update(GrainTicket gs) {
        boolean resultado;
        try {
            JPA.beginTransaction();
            resultado = JPA.update(gs);
            JPA.commitTransaction();
        } catch (Exception ex) {
            resultado = false;
        } finally {
            JPA.rollbackTransaction();
        }
        return resultado;
    }

    /**
     *
     * @param num
     * @return
     */
    public static GrainTicket find(String num) {
        try {
            return (GrainTicket) JPA.runQuery("SELECT FROM " + GrainTicket.class.getName() + " gt WHERE gt.nro='" + num + "'").get(0);
        } catch (Exception ex) {
        }
        return null;
    }

    /**
     *
     * @param empresa
     * @param num
     * @return
     */
    public static GrainTicket find(Enterprise empresa, String num) {
        try {
            return (GrainTicket) JPA.runQuery("SELECT FROM " + GrainTicket.class.getName() + " gt WHERE enterprise.id=" + empresa.getId() + " AND gt.nro='" + num + "'").get(0);
        } catch (Exception ex) {
        }
        return null;
    }

    /**
     *
     * @return
     */
    public static List getAll() {
        try {
            return JPA.getAll(GrainTicket.class);
        } catch (Exception ex) {
        }
        return null;
    }

    /**
     *
     * @param c
     * @return
     */
    public static List<GrainTicket> getByClient(Client c) {
        String consulta = "SELECT gt FROM " + GrainTicket.class.getName() + " gt WHERE gt.client.id=" + c.getId();
        ArrayList<GrainTicket> resultado = new ArrayList();
        System.out.println(consulta);
        List resultadoAux = JPA.runQuery(consulta);
        if (resultadoAux != null) {
            for (int i = 0; i < resultadoAux.size(); i++) {
                GrainTicket object = (GrainTicket) resultadoAux.get(i);
                resultado.add(object);
            }
        }
        return resultado;
    }

    /**
     *
     * @param c
     * @param e
     * @return
     */
    public static List getByClientAndEnterprise(Client c, Enterprise e) {
        List all = c.getGrainTickets();
        LinkedList<GrainTicket> ret = new LinkedList();
        for (Object gt : all) {
            Enterprise aux = ((GrainTicket) gt).getEnterprise();
            if (aux == e) {
                ret.add((GrainTicket) gt);
            }
        }
        return ret;
    }

    /**
     *
     * @param cliente
     * @param enterprise
     * @param compNumber
     * @param tcomp
     * @return
     */
    public static ArrayList<GrainTicket> findTicket(Client cliente, Enterprise enterprise, String compNumber, Integer tcomp) {
        String consulta = "SELECT a FROM " + GrainTicket.class.getName() + " a WHERE ";
        String where = "";
        if (enterprise != null) {
            where += " a.enterprise.id=" + enterprise.getId();
        }
        if (compNumber != null) {
            if (compNumber.length() == 16) {
                where += " AND a.nro='" + compNumber + "'";
            }
        }
        if (cliente != null) {
            where += " AND a.client.id=" + cliente.getId() + "";
        }
        if (tcomp != null) {
            where += " AND a.type.id=" + tcomp;
        }
        consulta += where.startsWith(" AND") ? where.substring(4) : where;
        ArrayList<GrainTicket> resultado = new ArrayList();
        System.out.println(consulta);
        List resultadoAux = JPA.runQuery(consulta);
        if (resultadoAux != null) {
            for (int i = 0; i < resultadoAux.size(); i++) {
                GrainTicket object = (GrainTicket) resultadoAux.get(i);
                resultado.add(object);
            }
        }
        return resultado;
    }

    /**
     *
     * @param cliente
     * @param enterprise
     * @param compNumber
     * @param tcomp
     * @return
     */
    public static boolean exists(Client cliente, Enterprise enterprise, String compNumber, Integer tcomp) {
        if (findTicket(cliente, enterprise, compNumber, tcomp).size() <= 0) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * @param b
     */
    public static void main(String[] b) {
    }
}
