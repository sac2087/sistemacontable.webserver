/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import db.JPA;
import model.User;

/**
 *
 * @author gdf
 */
public class LoginController {

    /**
     * validate user and create session
     *
     * @param userName
     * @param pass
     * @return true when user exists in database
     */
    public static boolean validate(String userName, String pass) {
        try {
            User u = (User) JPA.findByID(User.class, userName);
            crypt cr = new crypt();
            String decryptedPass = "";
            try {
                decryptedPass = cr.decrypt(u.getPass());
            } catch (Exception e) {
                return false;
            }
            if (pass.compareTo(decryptedPass) == 0) {
                model.Session.user = u;
                return true;
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
