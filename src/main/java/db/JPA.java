/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author gdf
 */
public class JPA {

    private static EntityManager em = DbConnection.getInstance();
    private static EntityTransaction tx = em.getTransaction();

    /**
     *
     */
    public static void beginTransaction() {
        tx.begin();
    }

    /**
     *
     */
    public static void commitTransaction() {
        tx.commit();
    }

    /**
     *
     */
    public static void rollbackTransaction() {
        if (tx.isActive()) {
            tx.rollback();
        }
    }

    /**
     *
     * @param type
     * @param id
     * @return
     */
    public static Object findByID(Class type, Object id) {
        Object obj = null;
        try {
            obj = em.find(type, id);
        } catch (Exception e) {
            System.out.println("Eror al buscar");
        }
        return obj;
    }

    /**
     *
     * @param t
     * @param name
     * @return
     */
    public static List<Object> findByName(Class t, String name) {
        try {
            return em.createQuery("SELECT e FROM " + t.getName() + " e WHERE name='" + name + "' ").getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param t
     * @param field
     * @param value
     * @return
     */
    public static List<Object> findByField(Class t, String field, String value) {
        try {
            return em.createQuery("SELECT e FROM " + t.getName() + " e WHERE " + field + "='" + value + "' ").getResultList();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param query
     * @return
     */
    public static List<Object> runQuery(String query) {
        try {
            return em.createQuery(query).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("entroo al catch");
            return null;
        }
    }

    /**
     *
     * @param obj
     * @return
     */
    public static boolean update(Object obj) {
        try {
            em.merge(obj);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     *
     * @param t
     * @return
     */
    public static List<Object> getAll(Class t) {
        try {
            return em.createQuery("SELECT e FROM " + t.getName() + " e").getResultList();
        } catch (Exception e) {
            return null;
        }
    }
}
