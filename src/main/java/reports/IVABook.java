/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package reports;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import controller.ClientController;
import controller.GrainTicketController;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import model.Account;
import model.Client;
import model.GrainTicket;
import model.GrainTicketAccount;
import static model.TConcepto.BRUTO;
import static model.TConcepto.IVA;
import static model.TConcepto.NETO;
import static model.TConcepto.NO_GRAVADO;
import static model.TConcepto.RET_PERCEPCIONES;
import model.TOperacion;
import model.Ticket;
import model.TicketAccount;

/**
 *
 * @author lprone
 */
public class IVABook {

    /**
     *
     * @param file
     * @param top
     */
    public void createPdf(String file, Client c, TOperacion top) {
        /*Declaramos documento como un objeto Document
         Asignamos el tama�o de hoja y los margenes */
        Document documento = new Document(PageSize.A4, 0, 0, 15, 0);

        //writer es declarado como el m�todo utilizado para escribir en el archivo
        PdfWriter writer = null;

        try {
            //Obtenemos la instancia del archivo a utilizar
            writer = PdfWriter.getInstance(documento, new FileOutputStream(file));
            Rectangle rct = new Rectangle(PageSize.A4.getRight() - 5, PageSize.A4.getTop() - 15);
            //Definimos un nombre y un tama�o para el PageBox los nombres posibles son: �crop�, �trim�, �art� and �bleed�.
            writer.setBoxSize("art", rct);
            PdfPageEventHelper event = new IVABook.PageNumber();
            writer.setPageEvent(event);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //Abrimos el documento para edici�n
        documento.open();

        try {
            //Obtenemos la instancia de la imagen
            Image imagen = Image.getInstance(this.getClass().getResource("/img/logo_pdf.png"));
            //Alineamos la imagen 
            imagen.setAlignment(Image.ALIGN_LEFT);
            //Escalamos la imagen 
            imagen.scalePercent(50);
            //Agregamos la imagen al documento
            documento.add(imagen);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Paragraph title = new Paragraph();
        title.setAlignment(Paragraph.ALIGN_CENTER);
        title.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 16));
        title.add("LIBRO IVA " + top.name() + "S");

        Paragraph client = new Paragraph();
        client.setAlignment(Paragraph.ALIGN_CENTER);
        title.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 14));
        client.add(c.getName() + "\n");
        client.add(c.getCuit());

        try {
            documento.add(title);
            documento.add(client);
            documento.add(new Paragraph(" "));
            documento.add(tabla(top, c));
        } catch (DocumentException ex) {
            ex.printStackTrace();
        }

        documento.close(); //Cerramos el documento
        writer.close(); //Cerramos writer
    }

    /**
     *
     * @param value
     * @return
     */
    private static PdfPCell cellText(String value) {
        Paragraph cellTitle = new Paragraph();
        cellTitle.setAlignment(Paragraph.ALIGN_CENTER);
        cellTitle.setFont(new Font(Font.FontFamily.HELVETICA, 6));
        cellTitle.add(value);
        PdfPCell cell = new PdfPCell(cellTitle);
        cell.setHorizontalAlignment(Element.ALIGN_CENTER);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    private static PdfPCell cellNumber(String value) {
        Paragraph cellTitle = new Paragraph();
        cellTitle.setAlignment(Paragraph.ALIGN_CENTER);
        cellTitle.setFont(new Font(Font.FontFamily.HELVETICA, 6));
        cellTitle.add(value);
        PdfPCell cell = new PdfPCell(cellTitle);
        cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
        cell.setBorder(Rectangle.NO_BORDER);
        return cell;
    }

    /**
     *
     * @param value
     * @return
     */
    private static PdfPCell cellTitle(String value) {
        Paragraph title = new Paragraph();
        title.setAlignment(Paragraph.ALIGN_CENTER);
        title.setFont(new Font(Font.FontFamily.TIMES_ROMAN, 10));
        title.add(value);
        PdfPCell cellTitle = new PdfPCell(title);
        cellTitle.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellTitle.setVerticalAlignment(Element.ALIGN_MIDDLE);
        cellTitle.setBorder(Rectangle.BOTTOM);
        return cellTitle;
    }

    /**
     *
     * @param top
     * @return
     */
    private static PdfPTable tabla(TOperacion top, Client c) {
        PdfPTable tabla = new PdfPTable(10);

        tabla.setWidthPercentage(100);

        tabla.addCell(cellTitle("FECHA"));
        tabla.addCell(cellTitle("T. CPTE"));
        tabla.addCell(cellTitle("N� CPTE"));
        tabla.addCell(cellTitle(top == TOperacion.COMPRA ? "PROV" : "CLIENTE"));
        tabla.addCell(cellTitle("CUIT"));
        tabla.addCell(cellTitle("NETO"));
        tabla.addCell(cellTitle("NO GRAB\nEXENTO"));
        tabla.addCell(cellTitle("IVA"));
        tabla.addCell(cellTitle("RET\nPERCEP"));
        tabla.addCell(cellTitle("TOTAL"));

        tabla.setHeaderRows(1);

        ArrayList<Ticket> tickets = new ArrayList<Ticket>();
        for (Account a : c.getAccountPlan().getAccounts()) {

            for (TicketAccount ta : a.getTicket_Accounts()) {
                Ticket t = ta.getTicket();
                if (t.gettOp() == top) {
                    tickets.add(t);
                }
            }
        }
        HashSet<Ticket> hsTickets = new HashSet<Ticket>();
        hsTickets.addAll(tickets);
        tickets.clear();
        tickets.addAll(hsTickets);
        Collections.sort(tickets);
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        ArrayList<GrainTicket> grainTickets = new ArrayList<GrainTicket>();

        grainTickets.addAll(GrainTicketController.getByClient(c));
        System.out.println(c.getId());
        System.out.println(grainTickets.size());
        HashSet<GrainTicket> hsGrainTickets = new HashSet<GrainTicket>();
        for (GrainTicket grainTicket : grainTickets) {
            if (top == TOperacion.VENTA) {
                if (grainTicket.getType().getId() == 97 || grainTicket.getType().getId() == 98) {
                    hsGrainTickets.add(grainTicket);
                }
            } else {
                hsGrainTickets.add(grainTicket);
            }
        }
        grainTickets.clear();
        grainTickets.addAll(hsGrainTickets);
        Collections.sort(grainTickets);

        boolean end = tickets.isEmpty() || grainTickets.isEmpty();

        while (!end) {
            if (!tickets.isEmpty() && !grainTickets.isEmpty()) {
                Ticket ticket = tickets.get(0);
                GrainTicket grainTicket = grainTickets.get(0);
                if (ticket.getEmissionDate().compareTo(grainTicket.getEmissionDate()) >= 0) {
                    tabla.addCell(cellText(sdf.format(grainTicket.getEmissionDate())));
                    tabla.addCell(cellText(grainTicket.getType().getName().trim()));
                    tabla.addCell(cellText(grainTicket.getNro()));
                    tabla.addCell(cellText(grainTicket.getEnterprise().getName().trim()));
                    try {
                        String mask = c.getCuit().length() > 8 ? "##.##.###.###.#" : "##.###.###";
                        tabla.addCell(cellText(formatString(grainTicket.getEnterprise().getCuit().trim(), mask)));
                    } catch (ParseException ex) {
                        tabla.addCell(cellText(grainTicket.getEnterprise().getCuit().trim()));
                    }
                    BigDecimal neto = BigDecimal.ZERO;
                    BigDecimal ungrav = BigDecimal.ZERO;
                    BigDecimal iva = BigDecimal.ZERO;
                    BigDecimal bruto = BigDecimal.ZERO;
                    BigDecimal ret = BigDecimal.ZERO;
                    for (GrainTicketAccount ta : grainTicket.getTicket_Accounts()) {
                        if (top == TOperacion.COMPRA) {
                            switch (ta.getConcepto()) {
                                case NETOC:
                                    neto = neto.add(ta.getPrice().multiply(new BigDecimal(ta.getKg())));
                                    break;
                                case IVAC:
                                    iva = iva.add(ta.getPrice());
                                    break;
                                case NO_GRAVADOC:
                                    ungrav = ungrav.add(ta.getPrice());
                                    break;
                                case BRUTOC:
                                    bruto = bruto.add(ta.getPrice());
                                    break;
                                case RET_PERCEPCIONESC:
                                    ret = ret.add(ta.getPrice());
                                    break;
                            }
                        } else {
                            switch (ta.getConcepto()) {
                                case NETOV:
                                    neto = neto.add(ta.getPrice().multiply(new BigDecimal(ta.getKg())));
                                    break;
                                case IVAV:
                                    iva = iva.add(ta.getPrice());
                                    break;
                                case NO_GRAVADOV:
                                    ungrav = ungrav.add(ta.getPrice());
                                    break;
                                case BRUTOV:
                                    bruto = bruto.add(ta.getPrice());
                                    break;
                                case RET_PERCEPCIONESV:
                                    ret = ret.add(ta.getPrice());
                                    break;
                            }
                        }
                    }
                    tabla.addCell(cellNumber(neto.toString().trim()));
                    tabla.addCell(cellNumber(ungrav.toString().trim()));
                    tabla.addCell(cellNumber(iva.toString().trim()));
                    tabla.addCell(cellNumber(ret.toString().trim()));
                    tabla.addCell(cellNumber(bruto.toString().trim()));
                    grainTickets.remove(0);
                } else {
                    tabla.addCell(cellText(sdf.format(ticket.getEmissionDate())));
                    tabla.addCell(cellText(ticket.getType().getName().trim()));
                    tabla.addCell(cellText(ticket.getSucursal() + "-" + ticket.getNumber()));
                    tabla.addCell(cellText(ticket.getEnterprise().getName().trim()));
                    try {
                        String mask = c.getCuit().length() > 8 ? "##.##.###.###.#" : "##.###.###";
                        tabla.addCell(cellText(formatString(ticket.getEnterprise().getCuit().trim(), mask)));
                    } catch (ParseException ex) {
                        tabla.addCell(cellText(ticket.getEnterprise().getCuit().trim()));
                    }
                    BigDecimal neto = BigDecimal.ZERO;
                    BigDecimal ungrav = BigDecimal.ZERO;
                    BigDecimal iva = BigDecimal.ZERO;
                    BigDecimal bruto = BigDecimal.ZERO;
                    BigDecimal ret = BigDecimal.ZERO;
                    for (TicketAccount ta : ticket.getTicket_Accounts()) {
                        switch (ta.getConcepto()) {
                            case NETO:
                                neto = neto.add(ta.getPrice());
                                break;
                            case IVA:
                                iva = iva.add(ta.getPrice());
                                break;
                            case NO_GRAVADO:
                                ungrav = ungrav.add(ta.getPrice());
                                break;
                            case BRUTO:
                                bruto = bruto.add(ta.getPrice());
                                break;
                            case RET_PERCEPCIONES:
                                ret = ret.add(ta.getPrice());
                                break;
                        }
                    }
                    tabla.addCell(cellNumber(neto.toString().trim()));
                    tabla.addCell(cellNumber(ungrav.toString().trim()));
                    tabla.addCell(cellNumber(iva.toString().trim()));
                    tabla.addCell(cellNumber(ret.toString().trim()));
                    tabla.addCell(cellNumber(bruto.toString().trim()));
                    tickets.remove(0);
                }
            }
            end = tickets.isEmpty() || grainTickets.isEmpty();
        }

        for (Ticket ticket : tickets) {
            tabla.addCell(cellText(sdf.format(ticket.getEmissionDate())));
            tabla.addCell(cellText(ticket.getType().getName().trim()));
            tabla.addCell(cellText(ticket.getSucursal() + "-" + ticket.getNumber()));
            tabla.addCell(cellText(ticket.getEnterprise().getName().trim()));
            try {
                String mask = c.getCuit().length() > 8 ? "##.##.###.###.#" : "##.###.###";
                tabla.addCell(cellText(formatString(ticket.getEnterprise().getCuit().trim(), mask)));
            } catch (ParseException ex) {
                tabla.addCell(cellText(ticket.getEnterprise().getCuit().trim()));
            }
            BigDecimal neto = BigDecimal.ZERO;
            BigDecimal ungrav = BigDecimal.ZERO;
            BigDecimal iva = BigDecimal.ZERO;
            BigDecimal bruto = BigDecimal.ZERO;
            BigDecimal ret = BigDecimal.ZERO;
            for (TicketAccount ta : ticket.getTicket_Accounts()) {
                switch (ta.getConcepto()) {
                    case NETO:
                        neto = neto.add(ta.getPrice());
                        break;
                    case IVA:
                        iva = iva.add(ta.getPrice());
                        break;
                    case NO_GRAVADO:
                        ungrav = ungrav.add(ta.getPrice());
                        break;
                    case BRUTO:
                        bruto = bruto.add(ta.getPrice());
                        break;
                    case RET_PERCEPCIONES:
                        ret = ret.add(ta.getPrice());
                        break;
                }
            }
            tabla.addCell(cellNumber(neto.toString().trim()));
            tabla.addCell(cellNumber(ungrav.toString().trim()));
            tabla.addCell(cellNumber(iva.toString().trim()));
            tabla.addCell(cellNumber(ret.toString().trim()));
            tabla.addCell(cellNumber(bruto.toString().trim()));
        }

        for (GrainTicket grainTicket : grainTickets) {
            tabla.addCell(cellText(sdf.format(grainTicket.getEmissionDate())));
            tabla.addCell(cellText(grainTicket.getType().getName().trim()));
            tabla.addCell(cellText(grainTicket.getNro()));
            tabla.addCell(cellText(grainTicket.getEnterprise().getName().trim()));
            try {
                String mask = c.getCuit().length() > 8 ? "##.##.###.###.#" : "##.###.###";
                tabla.addCell(cellText(formatString(grainTicket.getEnterprise().getCuit().trim(), mask)));
            } catch (ParseException ex) {
                tabla.addCell(cellText(grainTicket.getEnterprise().getCuit().trim()));
            }
            BigDecimal neto = BigDecimal.ZERO;
            BigDecimal ungrav = BigDecimal.ZERO;
            BigDecimal iva = BigDecimal.ZERO;
            BigDecimal bruto = BigDecimal.ZERO;
            BigDecimal ret = BigDecimal.ZERO;
            for (GrainTicketAccount ta : grainTicket.getTicket_Accounts()) {
                if (top == TOperacion.COMPRA) {
                    switch (ta.getConcepto()) {
                        case NETOC:
                            neto = neto.add(ta.getPrice().multiply(new BigDecimal(ta.getKg())));
                            break;
                        case IVAC:
                            iva = iva.add(ta.getPrice());
                            break;
                        case NO_GRAVADOC:
                            ungrav = ungrav.add(ta.getPrice());
                            break;
                        case BRUTOC:
                            bruto = bruto.add(ta.getPrice());
                            break;
                        case RET_PERCEPCIONESC:
                            ret = ret.add(ta.getPrice());
                            break;
                    }
                } else {
                    switch (ta.getConcepto()) {
                        case NETOV:
                            neto = neto.add(ta.getPrice().multiply(new BigDecimal(ta.getKg())));
                            break;
                        case IVAV:
                            iva = iva.add(ta.getPrice());
                            break;
                        case NO_GRAVADOV:
                            ungrav = ungrav.add(ta.getPrice());
                            break;
                        case BRUTOV:
                            bruto = bruto.add(ta.getPrice());
                            break;
                        case RET_PERCEPCIONESV:
                            ret = ret.add(ta.getPrice());
                            break;
                    }
                }
            }
            tabla.addCell(cellNumber(neto.toString().trim()));
            tabla.addCell(cellNumber(ungrav.toString().trim()));
            tabla.addCell(cellNumber(iva.toString().trim()));
            tabla.addCell(cellNumber(ret.toString().trim()));
            tabla.addCell(cellNumber(bruto.toString().trim()));
        }


        tabla.setComplete(true);
        return tabla;
    }

    /**
     *
     * @param string
     * @param mask
     * @return
     * @throws java.text.ParseException
     */
    public static String formatString(String string, String mask) throws java.text.ParseException {
        javax.swing.text.MaskFormatter mf = new javax.swing.text.MaskFormatter(mask);
        mf.setValueContainsLiteralCharacters(false);
        return mf.valueToString(string);
    }

    /**
     *
     */
    static class PageNumber extends PdfPageEventHelper {

        /**
         *
         * @param writer
         * @param document
         */
        @Override
        public void onEndPage(PdfWriter writer, Document document) {
            Rectangle r = writer.getBoxSize("art");
            Paragraph page = new Paragraph("Pagina " + writer.getPageNumber());
            page.setFont(new Font(Font.FontFamily.HELVETICA, 10));
            ColumnText.showTextAligned(writer.getDirectContent(), Element.ALIGN_RIGHT, page, r.getRight(), r.getTop(), 0);
        }
    }

    /**
     *
     * @param a
     */
    public static void main(String[] a) {
        String fullPath = "C:/Users/Usuario/Desktop/iva.pdf";
        new reports.IVABook().createPdf(fullPath, (Client) ClientController.getAll().get(0), TOperacion.COMPRA);
        try {
            File path = new File(fullPath);
            Desktop.getDesktop().open(path);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
